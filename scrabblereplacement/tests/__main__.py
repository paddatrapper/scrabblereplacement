import unittest
from scrabblereplacement import scrabblereplacement

DATA = 'data/corncob_lowercase.txt'


class TestFileParser(unittest.TestCase):
    def test_no_match(self):
        result = scrabblereplacement.parse_file(DATA, ['a'])
        self.assertListEqual(result['a'], [])

    def test_single_word(self):
        result = scrabblereplacement.parse_file(DATA, ['lightly'])
        for item in result['lightly']:
            self.assertEqual(len('lightly'), len(item))
            self.assertEqual('l', item[0])

    def test_sentence(self):
        sentence = 'lightly fried fish are delicious'.split(' ')
        result = scrabblereplacement.parse_file(DATA, sentence)
        for key in sentence:
            for item in result[key]:
                self.assertEqual(len(key), len(item))
                self.assertEqual(key[0], item[0])


class TestChooseWord(unittest.TestCase):
    def test_empty(self):
        result = scrabblereplacement.choose_word([], 'default')
        self.assertEqual(result, 'default')

    def test_list_not_default(self):
        for i in range(20):
            result = scrabblereplacement.choose_word(['word'], 'default')
            self.assertEqual(result, 'word')

    def test_list(self):
        for i in range(20):
            sentence = 'a long sentence'.split(' ')
            result = scrabblereplacement.choose_word(sentence, 'default')
            self.assertTrue(result in sentence)


if __name__ == '__main__':
    unittest.main()
