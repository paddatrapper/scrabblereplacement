import argparse
from .scrabblereplacement import parse_file, choose_word

DATA = 'data/corncob_lowercase.txt'

if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('sentence', help='The sentence to find replacements for. Quote the sentence to avoid shell-parsing issues. For example: "This is a test sentence"')
    args = parser.parse_args()
    mapping = parse_file(DATA, args.sentence.lower().split(' '))

    result = ''
    for key, value in mapping.items():
        result = f'{result}{choose_word(value, key)} '
    print(result.strip())
