import random


def parse_file(filename, words):
    """
    Reads in a file and returns a dict of words within that start with the same
    letter as a word in words and have the same length.

    Parameters:
    - filename: the file to read
    - words: a list of words to match

    Returns:
    A dict of words that match. The keys are the words provided and each key
    has a list of words if there are any. No matches result in an empty list.
    """
    result = {}
    first_letters = []
    for word in words:
        if word[0] not in first_letters:
            first_letters.append(word[0])
        result[word] = []

    with open(filename, 'r') as fp:
        for line in fp:
            line = line.strip()
            if line[0] in first_letters:
                for key in result:
                    if key[0] == line[0] and len(key) == len(line):
                        result[key].append(line)

    return result


def choose_word(words, default):
    """
    Makes a random selection from a list of words, defaulting to a given value
    if no words exist in the list

    Parameters:
    - words: The list of words to choose from
    - default: The default option to use if no options exist

    Returns:
    A word selected randomly from the options or the default if the list is
    empty
    """
    if words == []:
        return default

    return random.choice(words)
