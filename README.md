# Scrabble Replacement

Scrabble Replacement is a application for replacing a sentence with a set of
words with the same starting letter and length. This is helpful for Scrabble
training.

## Running
Scrabble Replacement is a python application with no special requirements. It
can be run with

```
git clone https://gitlab.com/paddatrapper/scrabblereplacement
cd scrabblereplacement
python3 -m scrabblereplacement "Your sentence here"
```

The sentence should be quoted to avoid shell expansion and incorrect parsing.
Help text for the command line interface can be viewed by

```
python3 -m scrabblereplacement -h
```

If `git` or `python3` are not found they can be installed on Debian/Ubuntu
systems using

```
sudo apt install git python3
```

Instructions for other platforms can be found on the official websites for
[git](https://git-scm.com/) and [Python](https://www.python.org/).

## Testing
Scrabble Replacement uses Python's built in `unittest` framework for unit
testing. It can be run using

```
python3 -m scrabblereplacement.tests
```

## Notes

This implementation seeks to reduce execution time by optimizing the number of
loops required. This is done through only looping through the given sentence
when necessary. Each line of the text file is checked against the set of unique
first letters in the sentence and only if that matches, will it find the full
word and compare length.
